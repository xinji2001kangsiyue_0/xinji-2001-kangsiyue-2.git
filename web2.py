import time

from selenium import webdriver
from selenium.webdriver.common.by import By
class TestCESHIREN():
    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    def test_cESHIREN(self):
        self.driver.get("https://ceshiren.com/")
        time.sleep(6)
        self.driver.set_window_size(1060, 816)
        #定位搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search").click()
        self.driver.find_element(By.ID, "search-term").click()
        #输入关键词
        self.driver.find_element(By.ID, "search-term").send_keys("康思悦")
        time.sleep(6)
        #点击按钮操作
        self.driver.find_element(By.CSS_SELECTOR, ".item:nth-child(1) .user-result").click()